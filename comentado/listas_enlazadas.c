#include <stdio.h>
#include <stdlib.h>

struct Nodo{
    int numero;
    struct Nodo *next;
};
typedef struct Nodo Nodo;

/*************************************************
*                                                *
*                     TO-DO                      *
*                                                *
*************************************************/

//Funciones básicas lista enlazada

void insertarInicio(Nodo *head, int num){
    /*
	    Inserta un nuevo nodo con numero num al inicio de la lista
	    enlazada que comienza en *head.
    */
    //Crear el nuevo nodo
    //Asignarle el valor num al nuevo nodo
    //Asignar a nuevo.siguiente el nodo head.siguiente
    //Asignar a head.siguiente el nuevo nodo
}

void insertarFinal(Nodo *head, int num){
    /*
	    Inserta un nuevo nodo con numero num al final de la lista
	    enlazada que comienza en *head.
    */
    //Crear el nuevo nodo
    //Asignarle el valor num al nuevo nodo
    //Buscar el ultimo nodo
    //Asignar a ultimo.next el nuevo nodo
}

int buscar(Nodo *head, int dato){
    /*
	    Busca en la lista *head el nodo con numero == dato.
	    Return
	    Cantidad de veces que se encontro el nodo
    */
    //Inicializar variable cantidad
    //Recorrer la lista completa (hasta que el siguiente sea null)
    //Por cada elemento cuyo numero sea igual a dato, cantidad++
}

int eliminar(Nodo *head, int dato){
    /*
	    Busca en la lista *head el primer nodo con numero == dato
	    y lo elimina de la lista.
	    Return
	    1: nodo eliminado
	    0: nodo no encontrado
    */
    //Recorrer la lista completa (hasta que el siguiente sea null)
    //Si se encuentra un elemento con numero == dato, then break
    //TIP: debe compararse el numero del elemento siguiente, para
    //     poder cambiar el puntero next del nodo anterior
    //Guardar el nodo a eliminar en una variable auxiliar Nodo *aux
    //Actualizar next del nodo anterior
    //Liberar memoria del nodo eliminado
}

//Funcion auxiliar
int extractFirst(Nodo *head){
}

//Funciones básicas cola FIFO

void encolar(Nodo *head, int dato){
	//Inserta al final
}

int desencolar(Nodo *head){
	//Extrae el primero
}

//Funciones básicas pila LIFO

void apilar(Nodo *head, int dato){
	//Inserta al inicio
}

int desapilar(Nodo *head){
	//Extrae el primero
}

//Funciones lista enlazada ordenada

void insertarOrdenado(Nodo *head, int num){
	/*
		Inserta un nuevo nodo con numero = num, en la
		posicion correspondiente segun orden
		ascendente.
	*/
	//Crear el nuevo nodo
    //Asignarle el valor num al nuevo nodo
    //Buscar un nodo cuyo numero sea mayor que num
    //Insertar el nuevo nodo antes de ese mayor
}

/*************************************************
*                                                *
*               WRAPPER FUNCTIONS                *
*                                                *
*************************************************/
void insertarInicioWrap(Nodo *head){
    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    insertarInicio(head, dato);
}

void insertarFinalWrap(Nodo *head){
    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    insertarFinal(head, dato);
}

int buscarWrap(Nodo *head){
    int response;

    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    response = buscar(head, dato);

    printf("El elemento %d se encuentra %d veces\n", dato, response);
}

int eliminarWrap(Nodo *head){
    int response;

    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    response = eliminar(head, dato);

    if (response)
        printf("El elemento %d se ha eliminado\n", dato);
    else
        printf("El elemento %d no existe en la lista\n", dato);
}

//Funciones cola

void encolarWrap(Nodo *head){
    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    encolar(head, dato);
}

void desencolarWrap(Nodo *head){
    int response = desencolar(head);
    if (response == -1) printf("Cola vacia\n");
    else printf("Elemento extraido: %d\n", response);
}

//Funciones pila

void apilarWrap(Nodo *head){
    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    apilar(head, dato);
}

void desapilarWrap(Nodo *head){
    int response = desapilar(head);
    if (response == -1) printf("Pila vacia\n");
    else printf("Elemento extraido: %d\n", response);
}

//Funciones lista enlazada ordenada

void insertarOrdenadoWrap(Nodo *head){
    int dato;
    printf("Dato: \n");
    scanf("%d", &dato);
    insertarOrdenado(head, dato);
}

/*************************************************
*                                                *
*               OTRAS FUNCIONES                  *
*                                                *
*************************************************/
void mostrarLista(Nodo *head){
    while (head->next != 0){
        printf("%d -> ", head->next->numero);
	head = head->next;
    }
    printf("||\n");
}

void vaciarLista(Nodo *head){
    if (head->next != 0){
        vaciarLista(head->next);
        free(head->next);
        head->next = 0;
    }
}

/*************************************************
*                                                *
*                      MENUS                     *
*                                                *
*************************************************/
void linkedList(){
    //Inicializar lista ensalada con nodo auxiliar
    Nodo *head;
    head = malloc(sizeof(Nodo));
    head->next = 0;

    int opcion;
    do{
        printf("\n***    Lista enlazada    ***\n");
        printf("(1) Insertar nodo al final\n");
        printf("(2) Insertar nodo al inicio\n");
        printf("(3) Buscar nodo\n");
        printf("(4) Eliminar nodo (primera aparicion)\n");
        printf("(5) Mostrar lista\n");
        printf("(6) Vaciar lista\n");
        printf("(0) Atrás (elimina todo)\n");
        
        printf("Opcion: \n");
        scanf("%d", &opcion);

        switch (opcion){
            default:
                break;
            case 1:
                insertarFinalWrap(head);
                mostrarLista(head);
                break;
            case 2:
                insertarInicioWrap(head);
                mostrarLista(head);
                break;
            case 3:
                buscarWrap(head);
                break;
            case 4:
                eliminarWrap(head);
                mostrarLista(head);
                break;
            case 5:
                mostrarLista(head);
                break;
            case 6:
                vaciarLista(head);
                break;
        }
    }while (opcion != 0);
}

void queue(){
    Nodo *head;
    head = malloc(sizeof(Nodo));
    head->next = 0;

    int opcion;
    do{
        printf("\n***    Cola (queue) - FIFO    ***\n");
        printf("(1) Encolar (enqueue)\n");
        printf("(2) Desencolar (dequeue)\n");
        printf("(3) Mostrar lista\n");
        printf("(4) Vaciar lista\n");
        printf("(0) Atrás (elimina todo)\n");
        
        printf("Opcion: \n");
        scanf("%d", &opcion);

        switch (opcion){
            default:
                break;
            case 1:
                encolarWrap(head);
                mostrarLista(head);
                break;
            case 2:
                desencolarWrap(head);
                mostrarLista(head);
                break;
            case 3:
                mostrarLista(head);
                break;
            case 4:
                vaciarLista(head);
                break;
        }
    }while (opcion != 0);
}

void stack(){
    Nodo *head;
    head = malloc(sizeof(Nodo));
    head->next = 0;

    int opcion;
    do{
        printf("\n***    Pila (stack) - LIFO    ***\n");
        printf("(1) Apilar (push)\n");
        printf("(2) Desapilar (pop)\n");
        printf("(3) Mostrar lista\n");
        printf("(4) Vaciar lista\n");
        printf("(0) Atrás (elimina todo)\n");
        
        printf("Opcion: \n");
        scanf("%d", &opcion);

        switch (opcion){
            default:
                break;
            case 1:
                apilarWrap(head);
                mostrarLista(head);
                break;
            case 2:
                desapilarWrap(head);
                mostrarLista(head);
                break;
            case 3:
                mostrarLista(head);
                break;
            case 4:
                vaciarLista(head);
                break;
        }
    }while (opcion != 0);
}

void sortedLinkedList(){
    Nodo *head;
    head = malloc(sizeof(Nodo));
    head->next = 0;

    int opcion;
    do{
        printf("\n***    Lista enlazada ordenada   ***\n");
        printf("(1) Insertar nodo\n");
        printf("(2) Buscar nodo\n");
        printf("(3) Eliminar nodo (primera aparicion)\n");
        printf("(4) Mostrar lista\n");
        printf("(5) Vaciar lista\n");
        printf("(0) Atrás (elimina todo)\n");
        
        printf("Opcion: \n");
        scanf("%d", &opcion);

        switch (opcion){
            default:
                break;
            case 1:
                insertarOrdenadoWrap(head);
                mostrarLista(head);
                break;
            case 2:
                buscarWrap(head);
                break;
            case 3:
                eliminarWrap(head);
                mostrarLista(head);
                break;
            case 4:
                mostrarLista(head);
                break;
            case 5:
                vaciarLista(head);
                break;
        }
    }while (opcion != 0);
}

int main(){
    
    printf("***********************************\n");
    printf("*     Bienvenido bebe <(uwu)>     *\n");
    printf("***********************************\n");

    //Menucito lindo precioso
    int opcion;
    do{
        printf("\n*** Selecciona tipo de lista ***\n");
        printf("(1) Lista enlazada ordinaria\n");
        printf("(2) Cola - First In First Out\n");
        printf("(3) Pila - Last In First Out\n");
        printf("(4) Lista enlazada ordenada\n");
        printf("(0) Salir\n");
        
        printf("Opcion: \n");
        scanf("%d", &opcion);
        switch (opcion){
            default:
                break;
            case 1:
                linkedList();
                break;
            case 2:
                queue();
                break;
            case 3:
                stack();
                break;
            case 4:
                sortedLinkedList();
                break;
        }
    }while (opcion != 0);
    return 0;
}