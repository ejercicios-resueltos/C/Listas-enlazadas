# Listas enlazadas
En este ejercicio se practica la construcción de algunos TDAs basados en listas enlazadas, específicamente:
- lista enlazada ordinaria,
- cola,
- pila, y
- lista enlazada ordenada.

Para esto se provee un menú desde el cual se pueden realizar las operaciones básicas (inserción, eliminación, búsqueda).

## To-Do
El ejercicio consiste en completar las funciones del TDA.